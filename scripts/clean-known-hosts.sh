#!/bin/bash

NETWORK_PREFIX="10.10.0."

for OCTET in $(seq 1 254); do
  ssh-keygen -f "$HOME/.ssh/known_hosts" -R ${NETWORK_PREFIX}${OCTET} 2>/dev/null
done
