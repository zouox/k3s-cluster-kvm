# DevOps Project: Kubernetes Cluster Deployment on KVM using Terraform

## Project Overview

This project aims to automate the creation and configuration of a cluster of virtual machines managed by KVM (Kernel-based Virtual Machine) using Terraform in order to deploy Kubernetes with an [Ansible playbook coming soon]().

## Table of Contents

- [DevOps Project: Kubernetes Cluster Deployment on KVM using Terraform](#devops-project-kubernetes-cluster-deployment-on-kvm-using-terraform)
  - [Project Overview](#project-overview)
  - [Table of Contents](#table-of-contents)
  - [Prerequisites](#prerequisites)
    - [Dependencies](#dependencies)
    - [Virtualization](#virtualization)
    - [Network](#network)
  - [Setup Instructions](#setup-instructions)
    - [Clone the repository](#clone-the-repository)
    - [Terraform Setup](#terraform-setup)
  - [Usage](#usage)
    - [Deploy the cluster](#deploy-the-cluster)
    - [Destroy the cluster](#destroy-the-cluster)

## Prerequisites
### Dependencies
Before you begin, ensure you have the following installed on your local machine:
- [Terraform](https://www.terraform.io/downloads.html) (tested version: 1.8.5)
- [libvirt](https://libvirt.org/), [virt-manager](https://virt-manager.org/) and [qemu](https://www.qemu.org/)

### Virtualization
Additionally, ensure that KVM is enabled on your machine. You can check if your system supports KVM by running:

```sh
# vmx=intel, svm=amd
egrep -c '(vmx|svm)' /proc/cpuinfo
```

If the result is greater than 0, your system supports KVM.

### Network
The `virbr1` network name shall be available as it will be used to create a bridge (otherwise, you may change the `cluster.tf` file).

## Setup Instructions
### Clone the repository
```bash
git clone https://gitlab.com/zouox/kubernetes-kvm
```

### Terraform Setup
You can choose a cloud image to deploy, format `.qcow2`.
Tested on:
- [Debian 12 (Cloud Generic)](https://cloud.debian.org/images/cloud/)
- [Almalinux 8 (Cloud Generic)](https://wiki.almalinux.org/cloud/Generic-cloud.html)

Rename `cloud_init.cfg.template` to `cloud_init.cfg` and replace the `<values>`.

Rename `variables.tf.template` to `variables.tf` and edit the variables:

| Variable         | Description                                                         |
| ---------------- | ------------------------------------------------------------------- |
| cp_vm_number     | Number of control planes VMs                                         |
| worker_vm_number | Number of workers VMs                                               |
| nfs_vm_number    | Number of NFS VMs (storage)                                         |
| virt_pool_path   | Path to the virt_pool (used for the source and destination volumes) |
| network_address  | Network IP address                                                  |
| network_id       | Network prefix of the IP address                                    |
| os               | OS name                                                             |
| image            | Source image (.qcow2)                                               |

## Usage
### Deploy the cluster
```bash
cd kubernetes-kvm
terraform init
terraform plan
terraform apply
# enter 'yes' to deploy
```

### Destroy the cluster
```bash
terraform destroy
./
```

Whenever you destroy the cluster, if you ever connected by SSH to the VMs, there will be fingerprints in the known hosts. You can clean them with this script : `script/clean-known-hosts.sh`.

:warning: Edit the variable `$NETWORK_PREFIX` with your network prefix IP address before use!