# Defining reference volume
resource "libvirt_volume" "reference_qcow2" {
  name = "${var.os}.qcow2"
  pool = "pool" # List storage pools using virsh pool-list
  source = "${var.virt_pool_path}/${var.image}"
  format = "qcow2"

}# Defining resized Control Planes volumes
resource "libvirt_volume" "resized_cp_qcow2" {
  count = "${var.cp_vm_number}"
  base_volume_id = libvirt_volume.reference_qcow2.id
  name = "${var.os}_resized_cp${count.index + 1}.qcow2"
  pool = "pool" # List storage pools using virsh pool-list
  size = 10737418240 #10Gb
  format = "qcow2"
}

# Defining Workers volumes
resource "libvirt_volume" "resized_worker_qcow2" {
  count = "${var.worker_vm_number}"
  base_volume_id = libvirt_volume.reference_qcow2.id
  name = "${var.os}_resized_worker${count.index + 1}.qcow2"
  pool = "pool"
  size = 10737418240 #10Gb
  format = "qcow2"
}

# Defining NFS volumes
resource "libvirt_volume" "resized_nfs_qcow2" {
  count = "${var.nfs_vm_number}"
  base_volume_id = libvirt_volume.reference_qcow2.id
  name = "${var.os}_resized_nfs${count.index + 1}.qcow2"
  pool = "pool"
  size = 10737418240 #10Gb
  format = "qcow2"
}

# Get user data info
data "template_file" "user_data" {
  template = "${file("cloud_init.cfg")}"
}

# Use CloudInit to add the instance
resource "libvirt_cloudinit_disk" "commoninit" {
  name = "commoninit.iso"
  pool = "pool"
  user_data      = "${data.template_file.user_data.rendered}"
}

resource "libvirt_network" "kubernetes-net" {
   name = "kubernetes-net"
   mode = "nat"
   bridge = "virbr1"
   addresses = ["${var.network_address}"]
   dns {
    enabled = true
   }
}

# Define KVM domain to create
resource "libvirt_domain" "cp" {
  count = "${var.cp_vm_number}"
  name   = "${var.os}_cp${count.index + 1}"
  memory = "2048"
  vcpu   = 2

  network_interface {
    network_id = libvirt_network.kubernetes-net.id
    hostname  = "cp${count.index + 1}"
    addresses = ["${var.network_id}${count.index + 11}"]
  }

  disk {
    volume_id = "${libvirt_volume.resized_cp_qcow2[count.index].id}"
  }

  cloudinit = "${libvirt_cloudinit_disk.commoninit.id}"

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}

resource "libvirt_domain" "worker" {
  count = "${var.worker_vm_number}"
  name   = "${var.os}_worker${count.index + 1}"
  memory = "4096"
  vcpu   = 2

  network_interface {
    network_id = libvirt_network.kubernetes-net.id
    hostname  = "worker${count.index + 1}"
    addresses = ["${var.network_id}${count.index + 21}"]
  }

  disk {
    volume_id = "${libvirt_volume.resized_worker_qcow2[count.index].id}"
  }

  cloudinit = "${libvirt_cloudinit_disk.commoninit.id}"

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}
resource "libvirt_domain" "nfs" {
  count = "${var.nfs_vm_number}"
  name   = "${var.os}_nfs${count.index + 1}"
  memory = "4096"
  vcpu   = 2

  network_interface {
    network_id = libvirt_network.kubernetes-net.id
    hostname  = "nfs${count.index + 1}"
    addresses = ["${var.network_id}${count.index + 31}"]
  }

  disk {
    volume_id = "${libvirt_volume.resized_nfs_qcow2[count.index].id}"
  }

  cloudinit = "${libvirt_cloudinit_disk.commoninit.id}"

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}
